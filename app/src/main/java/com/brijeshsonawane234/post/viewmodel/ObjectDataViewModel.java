package com.brijeshsonawane234.post.viewmodel;

import androidx.lifecycle.ViewModel;
import android.util.Log;

import com.brijeshsonawane234.post.Adapter.ObjectDataAdapter;
import com.brijeshsonawane234.post.Model.ObjectData;
import com.brijeshsonawane234.post.Remot.RetroClass;

import java.util.List;

public class ObjectDataViewModel extends ViewModel
{
    public List<ObjectData> objectData;
    public RetroClass retroClass = new RetroClass();

    public List<ObjectData> getObjectData(ObjectDataAdapter objectDataAdapter)
    {
        if(objectData == null)
        {
            objectData = retroClass.prepareData(objectDataAdapter);
        }
        Log.d("objectdatasize",""+objectData.size());
        return objectData;
    }
}
