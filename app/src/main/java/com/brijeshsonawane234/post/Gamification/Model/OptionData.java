package com.brijeshsonawane234.post.Gamification.Model;

public class OptionData
{
    String answerId;
    int answer;

    public OptionData() {
    }

    public OptionData(String answerId, int answer) {
        this.answerId = answerId;
        this.answer = answer;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }
}
