package com.brijeshsonawane234.post.Gamification.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.brijeshsonawane234.post.R;

public class DemoStart extends AppCompatActivity implements StartHowToPlay.BottomSheetListenerHowToPlay , StartTermsAndConditions.BottomSheetListenerTermsAndCondition
{
    ImageView iv1,iv2;
    Button b1,b2,start;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_start);
        b1=(Button) findViewById(R.id.btn1);
        b2=(Button) findViewById(R.id.btn2);
        iv1=(ImageView)findViewById(R.id.iv1);
        iv2=(ImageView)findViewById(R.id.iv2);
        start=(Button) findViewById(R.id.b2) ;
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DemoStart.this, ActivityQuiz.class));
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartHowToPlay bottomSheetHowToPlay = new StartHowToPlay();
                bottomSheetHowToPlay.show(getSupportFragmentManager(),"");
            }
        });
        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartHowToPlay bottomSheetHowToPlay = new StartHowToPlay();
                bottomSheetHowToPlay.show(getSupportFragmentManager(),"");
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartTermsAndConditions startTermsAndConditions = new StartTermsAndConditions();
                startTermsAndConditions.show(getSupportFragmentManager(),"");
            }
        });
        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartTermsAndConditions startTermsAndConditions = new StartTermsAndConditions();
                startTermsAndConditions.show(getSupportFragmentManager(),"");
            }
        });
    }

    @Override
    public void onButtonClicked(String text)
    {
        StartTermsAndConditions startTermsAndConditions = new StartTermsAndConditions();
        startTermsAndConditions.show(getSupportFragmentManager(),"");
    }

    @Override
    public void onButtonClickedTermsAndCondition(String text)
    {
        StartHowToPlay bottomSheetHowToPlay = new StartHowToPlay();
        bottomSheetHowToPlay.show(getSupportFragmentManager(),"");
    }
}
