package com.brijeshsonawane234.post.Gamification.Model;

public class Option
{
    String ans;

    public Option() {
    }

    public Option(String ans) {
        this.ans = ans;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }
}
