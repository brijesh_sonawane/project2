package com.brijeshsonawane234.post.Gamification.Model;

import java.util.List;

public class Question
{
    String question;
    List<Option> option;
    String correct;

    public Question(String question, List<Option> option, String correct) {
        this.question = question;
        this.option = option;
        this.correct = correct;
    }

    public Question() {
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Option> getOption() {
        return option;
    }

    public void setOption(List<Option> option) {
        this.option = option;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    @Override
    public String toString() {
        return "Question{" +
                "question='" + question + '\'' +
                ", option=" + option +
                ", correct='" + correct + '\'' +
                '}';
    }
}
