package com.brijeshsonawane234.post.Gamification.Model;

import java.util.List;

public class ObjectData
{
    int contestId;
    List<QuestionData> questions;

    public ObjectData() {
    }

    public ObjectData(int contestId, List<QuestionData> questions) {
        this.contestId = contestId;
        this.questions = questions;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public List<QuestionData> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionData> questions) {
        this.questions = questions;
    }
}
