package com.brijeshsonawane234.post.Gamification.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brijeshsonawane234.post.Gamification.Model.ClickId;
import com.brijeshsonawane234.post.R;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class demo extends AppCompatActivity {

    private Toolbar toolbar;
    TextView tv_question;
    ImageView imageViewHead;
    List<Button> buttons = new ArrayList<>();
    List<TextView> textViews = new ArrayList<>();
    int optionButton =-1,optionText=-1;
    int clickCount=0,currentQuestion=0;
    List<ClickId> id = new ArrayList<>();
    LinearLayout layoutText,layoutButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        toolbar = (Toolbar) findViewById(R.id.mytoolbar);
        tv_question = (TextView) findViewById(R.id.tv3);
        imageViewHead = (ImageView) findViewById(R.id.imageView1);

        layoutButton = (LinearLayout) findViewById(R.id.l_layout2_h);
        tv_question.setText("Lorem Ipsum is simply dummy text of the  printing and typesetting industy");
        setSupportActionBar(toolbar);

        addTextBox(8);

//        Button button=new Button(this);
//        button.setText("A");
//        button.setWidth(150);
//        layoutButton.addView(button);


        addButton(10,10);
    }
    private void addTextBox(int count)
    {
        int x,y;
        if(count > 6)
        {

            x=2;
            y=6;
        }
        else {
            x=1;
            y=count;
        }
        layoutText = (LinearLayout) findViewById(R.id.l_layout_h);
        for (int i = 0; i < x; i++) {
            LinearLayout row = new LinearLayout(this);
            row.setLayoutParams(new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT));
            row.setGravity(Gravity.CENTER);
            for (int j = 0; j < y; j++) {
                optionText++;
                final TextView textView = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(10, 10, 10, 10);
                textView.setLayoutParams(layoutParams);
                textView.setBackgroundResource(R.drawable.text_border);
                textView.setTextSize(18);
                textView.setText("");
                textView.setWidth(90);
                if (currentQuestion%2 == 0)
                {
                    textView.setTextColor(Color.rgb(0, 156, 226));
                    textView.setBackgroundResource(R.drawable.text_border);
                }
                else
                {
                    textView.setTextColor(Color.WHITE);
                    textView.setBackgroundResource(R.drawable.text_border2);
                }

                textView.setTypeface(Typeface.DEFAULT_BOLD);

                textView.setGravity(Gravity.CENTER);
                textView.setId(optionText);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        Log.d("tx",""+textView.getId());
                        clickCount=textView.getId();
                        //textViewString= (String) textViews.get(textView.getId()).getText();
                    }
                });
                row.addView(textView);
                textViews.add(textView);
            }
            y=count-6;
            layoutText.addView(row);
        }
    }

    public void addButton(int count,int number)
    {
        int x,y;
        if(count > 5)
        {
            x=2;
            y=5;
        }
        else {
            x=1;
            y=count;
        }
        layoutButton = (LinearLayout) findViewById(R.id.l_layout2_h);
        for (int i = 0; i < x; i++) {
            LinearLayout row = new LinearLayout(this);
            row.setLayoutParams(new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));
            row.setGravity(Gravity.CENTER);
            for (int j = 0; j < y; j++) {
                optionButton++;
                Button button= new Button(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100,LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(10,5,10,5);
                layoutParams.gravity=Gravity.CENTER;
                button.setLayoutParams(layoutParams);

                button.setTextSize(14);
                button.setText("A");
                //button.setWidth(10);

                if (currentQuestion%2 == 0)
                {
                    button.setTextColor(Color.WHITE);
                    button.setBackgroundResource(R.drawable.button_border2);
                }
                else
                {
                    button.setTextColor(Color.WHITE);
                    button.setBackgroundResource(R.drawable.button_border1);
                }
                button.setGravity(Gravity.CENTER);
//                button.setOnClickListener((View.OnClickListener) this);
                button.setId(optionButton);
                Log.d("buttonID","==="+optionButton);
                row.addView(button);
                buttons.add(button);
            }
            y=count-5;
            layoutButton.addView(row);
        }
    }
}
