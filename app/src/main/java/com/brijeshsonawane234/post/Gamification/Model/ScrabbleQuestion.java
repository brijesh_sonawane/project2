package com.brijeshsonawane234.post.Gamification.Model;

import java.util.List;

public class ScrabbleQuestion
{
    String image;
    List<ScrabbleCharacters> characters;
    String correct;

    public ScrabbleQuestion(String image, List<ScrabbleCharacters> characters, String correct) {
        this.image = image;
        this.characters = characters;
        this.correct = correct;
    }

    public ScrabbleQuestion() {
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<ScrabbleCharacters> getCharacters() {
        return characters;
    }

    public void setCharacters(List<ScrabbleCharacters> characters) {
        this.characters = characters;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }
}
