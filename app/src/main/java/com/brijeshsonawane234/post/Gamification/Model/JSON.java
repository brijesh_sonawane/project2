package com.brijeshsonawane234.post.Gamification.Model;

import java.util.List;

public class JSON
{
    String status;
    int code;
    ObjectData object;

    public JSON() {
    }

    public JSON(String status, int code, ObjectData object) {
        this.status = status;
        this.code = code;
        this.object = object;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ObjectData getObject() {
        return object;
    }

    public void setObject(ObjectData object) {
        this.object = object;
    }
}
