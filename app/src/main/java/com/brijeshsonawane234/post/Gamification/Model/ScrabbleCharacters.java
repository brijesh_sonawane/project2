package com.brijeshsonawane234.post.Gamification.Model;

public class ScrabbleCharacters
{
    String character;

    public ScrabbleCharacters(String character) {
        this.character = character;
    }

    public ScrabbleCharacters() {
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }
}
