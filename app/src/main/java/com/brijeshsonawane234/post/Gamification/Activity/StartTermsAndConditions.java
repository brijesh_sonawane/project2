package com.brijeshsonawane234.post.Gamification.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.brijeshsonawane234.post.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class StartTermsAndConditions extends BottomSheetDialogFragment
{
    BottomSheetListenerTermsAndCondition mListener;
    ImageView imageView1,imageView2;
    Button b1,b2;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v=inflater.inflate(R.layout.start_terms_and_condition,container,false);

        b1= v.findViewById(R.id.btn1);
        imageView1=v.findViewById(R.id.iv1);
        b2= v.findViewById(R.id.btn2);
        imageView2=v.findViewById(R.id.iv2);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onButtonClickedTermsAndCondition("click");
                dismiss();
            }
        });
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                mListener.onButtonClickedTermsAndCondition("click");
                dismiss();
            }
        });
        return v;
    }
    public interface BottomSheetListenerTermsAndCondition
    {
        void onButtonClickedTermsAndCondition(String text);
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        try {
            mListener = (BottomSheetListenerTermsAndCondition) context;
        }
        catch (ClassCastException e)
        {

        }
    }
}
