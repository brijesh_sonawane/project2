package com.brijeshsonawane234.post.Gamification.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.brijeshsonawane234.post.Model.Image;
import com.brijeshsonawane234.post.R;

import java.io.IOException;

public class ActivityWeb extends AppCompatActivity {

    WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        webView=(WebView) findViewById(R.id.wv1);
        webView.setWebViewClient(new WebViewClient());

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        Log.d("*********","*********");
        String file="file:///assets/start_native.svg";
        webView.loadUrl(file);
        //webView.loadUrl("http://www.google.com");
    }
}
