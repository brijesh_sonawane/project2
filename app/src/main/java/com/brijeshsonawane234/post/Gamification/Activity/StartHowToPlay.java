package com.brijeshsonawane234.post.Gamification.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.brijeshsonawane234.post.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class StartHowToPlay extends BottomSheetDialogFragment
{
    ImageView imageView1,imageView2;
    Button b1,b2;
    BottomSheetListenerHowToPlay mListener;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v=inflater.inflate(R.layout.start_how_to_play,container,false);
        b1= v.findViewById(R.id.btn1);
        imageView1=v.findViewById(R.id.iv1);
        b2= v.findViewById(R.id.btn2);
        imageView2=v.findViewById(R.id.iv2);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                mListener.onButtonClicked("click");
                dismiss();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                mListener.onButtonClicked("click");
                dismiss();
            }
        });
        return v;
    }

    public interface BottomSheetListenerHowToPlay
    {
        void onButtonClicked(String text);
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        try {
            mListener = (BottomSheetListenerHowToPlay) context;
        }
        catch (ClassCastException e)
        {

        }
    }
}
