package com.brijeshsonawane234.post.Gamification.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brijeshsonawane234.post.Gamification.Model.ClickId;
import com.brijeshsonawane234.post.Gamification.Model.ScrabbleCharacters;
import com.brijeshsonawane234.post.Gamification.Model.ScrabbleQuestion;
import com.brijeshsonawane234.post.R;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Main3Activity extends AppCompatActivity implements View.OnClickListener{

    private Toolbar toolbar;
    TextView tv_question,tv_appbar;
    ImageView imageViewHead,imageView2;
    List<Button> buttons = new ArrayList<>();
    List<TextView> textViews = new ArrayList<>();
    int optionButton =-1,optionText=-1;
    int clickCount=0;
    List<ClickId> id = new ArrayList<>();
    List<ScrabbleQuestion> scrabbleQuestions;
    int currentQuestion=0;
    LinearLayout layoutText,layoutButton,layout_parent,layout_appbar;
    String correctAns;
    int score=0;
    Button clearButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        toolbar = (Toolbar) findViewById(R.id.mytoolbar);
        tv_appbar = (TextView) findViewById(R.id.tv_appbar);
        tv_question = (TextView) findViewById(R.id.tv3);
        clearButton= (Button) findViewById(R.id.b1);
        imageViewHead = (ImageView) findViewById(R.id.imageView1);
        imageView2=(ImageView) findViewById(R.id.imageView2);
        layout_parent= (LinearLayout) findViewById(R.id.ll_parent);
        layout_appbar= (LinearLayout) findViewById(R.id.ll_appbar);
        tv_question.setText("Lorem Ipsum is simply dummy text of the  printing and typesetting industy");
        setSupportActionBar(toolbar);


        //get all questions
        loadAllQuestions();
//        Collections.shuffle(questions);
        //load first question
        setQuestionScreen(currentQuestion);


    }

    private void addTextBox(int count)
    {
        int x,y;
        if(count > 6)
        {

            x=2;
            y=6;
        }
        else {
            x=1;
            y=count;
        }
        layoutText = (LinearLayout) findViewById(R.id.l_layout_h);
        for (int i = 0; i < x; i++) {
            LinearLayout row = new LinearLayout(this);
            row.setLayoutParams(new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT));
            row.setGravity(Gravity.CENTER);
            for (int j = 0; j < y; j++) {
                optionText++;
                final TextView textView = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100,110);
                layoutParams.setMargins(10, 10, 10, 10);
                textView.setLayoutParams(layoutParams);
                textView.setBackgroundResource(R.drawable.text_border);
                textView.setTextSize(18);
                textView.setText("");
                textView.setWidth(90);
                textView.setGravity(Gravity.CENTER);
                if (currentQuestion%2 == 0)
                {
                    textView.setTextColor(Color.rgb(0, 156, 226));
                    textView.setBackgroundResource(R.drawable.text_border);
                }
                else
                {
                    textView.setTextColor(Color.WHITE);
                    textView.setBackgroundResource(R.drawable.text_border2);
                }

                textView.setTypeface(Typeface.DEFAULT_BOLD);

                textView.setGravity(Gravity.CENTER);
                textView.setId(optionText);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        Log.d("tx",""+textView.getId());
                        clickCount=textView.getId();
                        //textViewString= (String) textViews.get(textView.getId()).getText();
                    }
                });
                row.addView(textView);
                textViews.add(textView);
            }
            y=count-6;
            layoutText.addView(row);
        }
    }
    public void addButton(int count,int number)
    {
        int x,y;
        if(count > 5)
        {
            x=2;
            y=5;
        }
        else {
            x=1;
            y=count;
        }
        layoutButton = (LinearLayout) findViewById(R.id.l_layout2_h);
        for (int i = 0; i < x; i++) {
            LinearLayout row = new LinearLayout(this);
            row.setLayoutParams(new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));
            row.setGravity(Gravity.CENTER);
            for (int j = 0; j < y; j++) {
                optionButton++;
                Button button= new Button(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(110, ViewGroup.LayoutParams.WRAP_CONTENT);
                //layoutParams.setMargins(30,20,30,20);
                layoutParams.setMargins(10,5,10,5);
                layoutParams.gravity=Gravity.CENTER;
                button.setLayoutParams(layoutParams);

                button.setTextSize(14);
                button.setText(scrabbleQuestions.get(number).getCharacters().get(optionButton).getCharacter());

                if (currentQuestion%2 == 0)
                {
                    button.setTextColor(Color.WHITE);
                    button.setBackgroundResource(R.drawable.button_border2);
                }
                else
                {
                    button.setTextColor(Color.WHITE);
                    button.setBackgroundResource(R.drawable.button_border1);
                }
                button.setGravity(Gravity.CENTER);
                button.setOnClickListener((View.OnClickListener) this);
                button.setId(optionButton);
                Log.d("buttonID","==="+optionButton);
                row.addView(button);
                buttons.add(button);
            }
            y=count-5;
            layoutButton.addView(row);
        }
    }
    @Override
    public void onClick(View view)
    {

        Log.d("textViewSize",""+textViews.size());
        if (clickCount<textViews.size())
        {
            if (textViews.get(clickCount).getText().equals(""))
            {
                Log.d("clickCount", "===" + clickCount);
                Log.d("text", "===" + buttons.get(view.getId()));
                textViews.get(clickCount).setText(buttons.get(view.getId()).getText());
                buttons.get(view.getId()).setEnabled(false);
//                buttons.get(view.getId()).setBackgroundResource(R.drawable.block_button_border);
                buttons.get(view.getId()).setAlpha((float) 0.25);
                ClickId clickId=new ClickId(view.getId(),textViews.get(clickCount).getId());
                id.add(clickId);
                Log.d("clickId", "button===" + clickId.getButtonId());
                Log.d("clickId", "text===" + clickId.getTextId());
            }
            else
            {
                for(ClickId cId: id)
                {
                    Log.d("clickId", "button===" + cId.getButtonId());
                    Log.d("clickId", "text===" + cId.getTextId());
                    Log.d("clickId", "optionText===" + clickCount);

                    if(cId.getTextId()==clickCount)
                    {
                        Log.d("true","true");
                        int bId=cId.getButtonId();
                        textViews.get(clickCount).setText(buttons.get(view.getId()).getText());
                        buttons.get(bId).setEnabled(true);
                        buttons.get(bId).setAlpha(1);
//                        buttons.get(bId).setBackgroundResource(R.drawable.button_border);
                        int newBId=buttons.get(view.getId()).getId();
                        cId.setButtonId(newBId);
                        buttons.get(newBId).setEnabled(false);
                        buttons.get(newBId).setAlpha((float) 0.25);
//                        buttons.get(newBId).setBackgroundResource(R.drawable.block_button_border);
                    }
                }
            }
            clickCount++;
        }
        else
        {
            //empty
        }
    }


    //json file

    private void setQuestionScreen(int number)
    {
        //tv_question.setText(questions.get(number).getQuestion());
        id.clear();
        correctAns=scrabbleQuestions.get(number).getCorrect();
        final int textBoxCount=scrabbleQuestions.get(number).getCorrect().length();
        addTextBox(scrabbleQuestions.get(number).getCorrect().length());
        addButton(scrabbleQuestions.get(number).getCharacters().size(),number);
        if (currentQuestion<scrabbleQuestions.size()-1)
        {
            switch (currentQuestion)
            {
                case 0:
                    Glide.with(this).load(R.drawable.brand_logo).into(imageViewHead);
                    Glide.with(this).load(R.drawable.images).into(imageView2);
                    toolbar.setBackgroundColor(Color.WHITE);
                    layout_parent.setBackgroundResource(R.drawable.bg);
                    break;

                case 1: Glide.with(this).load(R.drawable.ola).into(imageView2);
                    layout_parent.setBackgroundResource(R.drawable.sacrable2bg);
                    toolbar.setBackgroundColor(Color.BLACK);
                    tv_appbar.setTextColor(Color.WHITE);
                    break;

                case 2: Glide.with(this).load(R.drawable.amazon).into(imageView2);
                    toolbar.setBackgroundColor(Color.WHITE);
                    layout_parent.setBackgroundResource(R.drawable.bg);
                    tv_appbar.setTextColor(Color.BLACK);
                    break;


                case 3:
                    Glide.with(this).load(R.drawable.javaiamge).into(imageView2);
                    layout_parent.setBackgroundResource(R.drawable.sacrable2bg);
                    toolbar.setBackgroundColor(Color.BLACK);
                    tv_appbar.setTextColor(Color.WHITE);
                    break;

            }
            new Timer().schedule(new TimerTask(){
                public void run() {
                    Main3Activity.this.runOnUiThread(new Runnable() {
                        public void run() {

                            Log.d("timer","timer1");
                            //flag=1;
                            String textBoxAns="";
                            for(int i=0;i<textBoxCount;i++)
                            {
                                Log.d("char","--"+textViews.get(i).getText());
                                String textBoxChar= (String) textViews.get(i).getText();
                                textBoxAns=textBoxAns.concat(textBoxChar);
                            }
                            Log.d("textBoxAns","=="+textBoxAns);
                            if (textBoxAns.equals(correctAns))
                            {
                                score++;
                                for (TextView tv: textViews)
                                {
                                    tv.setBackgroundColor(Color.GREEN);
                                }
                            }
                            else
                            {
                                for (TextView tv: textViews)
                                {
                                    tv.setBackgroundColor(Color.RED);
                                }
                            }
                            for (Button b: buttons)
                            {
                                b.setEnabled(false);

                            }
                            clearButton.setEnabled(false);
                            new Timer().schedule(new TimerTask(){
                                public void run() {
                                    Main3Activity.this.runOnUiThread(new Runnable() {
                                        public void run() {
                                            Log.d("timer","timer2");
                                            layoutText.removeAllViews();
                                            layoutButton.removeAllViews();
                                            buttons.clear();
                                            textViews.clear();
                                            optionButton=-1;
                                            optionText=-1;
                                            clickCount=0;
                                            currentQuestion++;
                                            clearButton.setEnabled(true);
                                            setQuestionScreen(currentQuestion);
                                        }
                                    });
                                }
                            }, 2000);

                        }
                    });
                }
            }, 20000);
        }
        else
        {
            Glide.with(this).load(R.drawable.amazon).into(imageView2);
            layout_parent.setBackgroundResource(R.drawable.sacrable2bg);
            toolbar.setBackgroundColor(Color.BLACK);
            tv_appbar.setTextColor(Color.WHITE);
            new Timer().schedule(new TimerTask(){
                public void run() {
                    Main3Activity.this.runOnUiThread(new Runnable() {
                        public void run() {

                            Log.d("timer","timer3");
                            String textBoxAns="";
                            for(int i=0;i<textBoxCount;i++)
                            {
                                Log.d("char","--"+textViews.get(i).getText());
                                String textBoxChar= (String) textViews.get(i).getText();
                                textBoxAns=textBoxAns.concat(textBoxChar);
                            }
                            Log.d("textBoxAns","=="+textBoxAns);
                            if (textBoxAns.equals(correctAns))
                            {
                                score++;
                                for (TextView tv: textViews)
                                {
                                    tv.setBackgroundColor(Color.GREEN);
                                }
                            }
                            else
                            {
                                for (TextView tv: textViews)
                                {
                                    tv.setBackgroundColor(Color.RED);
                                }
                            }
                            for (Button b: buttons)
                            {
                                b.setEnabled(false);
                            }
                            new Timer().schedule(new TimerTask(){
                                public void run() {
                                    Main3Activity.this.runOnUiThread(new Runnable() {
                                        public void run() {
                                            Log.d("timer","timer2");
                                            layoutText.removeAllViews();
                                            layoutButton.removeAllViews();
                                            buttons.clear();
                                            textViews.clear();
                                            optionButton=-1;
                                            optionText=-1;
                                            clickCount=0;
                                            currentQuestion++;
                                            startActivity(new Intent(Main3Activity.this, Main4Activity.class));
                                            Main3Activity.this.finish();
                                        }
                                    });
                                }
                            }, 2000);

                        }
                    });
                }
            }, 20000);
        }

    }


    private void loadAllQuestions()
    {
        scrabbleQuestions = new ArrayList<>();
        String jsonStr = loadJSONFromAsset("scrabble.json");
        Log.d("json1","in fun1");
        try
        {
            JSONObject jsonObject =  new JSONObject(jsonStr);
            Log.d("SizeOfJsonObject","====="+jsonObject);
            JSONArray questionss = jsonObject.getJSONArray("questions");

            for (int i=0;i< questionss.length();i++)
            {
                ScrabbleQuestion scrabbleQuestion=new ScrabbleQuestion();
                JSONObject question = questionss.getJSONObject(i);
                String image = question.getString("image");
                String correct = question.getString("correct");;
                Log.d("Image","====="+image);
                Log.d("correct","====="+correct);

                List<ScrabbleCharacters> scrabbleCharactersList= new ArrayList<>();
                JSONArray characters = question.getJSONArray("characters");
                Log.d("charcters","====="+characters);
                for (int j=0;j<characters.length();j++)
                {
                    ScrabbleCharacters scrabbleCharacters=new ScrabbleCharacters();
                    JSONObject charcter = characters.getJSONObject(j);
                    String character=charcter.getString("character");
                    scrabbleCharacters.setCharacter(character);
                    scrabbleCharactersList.add(scrabbleCharacters);
                }

                scrabbleQuestion.setImage(image);
                scrabbleQuestion.setCorrect(correct);
                scrabbleQuestion.setCharacters(scrabbleCharactersList);

                scrabbleQuestions.add(scrabbleQuestion);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String loadJSONFromAsset(String file)
    {
        //Log.d("infun","==    private String loadJSONFromAsset(String file)");
        String json="";
        try {
            InputStream is=getAssets().open("scrabble.json");
            int size= is.available();
            Log.d("size","size === "+size);
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json=new String(buffer,"UTF-8");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        Log.d("json1","in fun"+json.length());
        return json;
    }

    public void clearData(View view)
    {
        for (TextView t: textViews)
        {
            t.setText("");
        }
        for (Button b: buttons)
        {
            b.setEnabled(true);
            b.setAlpha(1);
        }
        optionButton=-1;
        optionText=-1;
        clickCount=0;
        id.clear();
    }
}
