package com.brijeshsonawane234.post.Gamification.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.brijeshsonawane234.post.R;

public class ActivityImageQuiz extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_image);
    }
    public void fun(View view)
    {
        startActivity(new Intent(ActivityImageQuiz.this, ActivityQuizMultipleText.class));
    }

}
