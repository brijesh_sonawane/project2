package com.brijeshsonawane234.post.Gamification.Model;

import java.util.List;

public class QuestionData
{
    String question;
    String questionId;
    List<OptionData> options;
    boolean isImage;
    boolean singleAnswer;
    String selectedAnswer;
    boolean answered;

    public QuestionData()
    {
    }

    public QuestionData(String question, String questionId, List<OptionData> options, boolean isImage, boolean singleAnswer, String selectedAnswer, boolean answered)
    {
        this.question = question;
        this.questionId = questionId;
        this.options = options;
        this.isImage = isImage;
        this.singleAnswer = singleAnswer;
        this.selectedAnswer = selectedAnswer;
        this.answered = answered;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public List<OptionData> getOptions() {
        return options;
    }

    public void setOptions(List<OptionData> options) {
        this.options = options;
    }

    public boolean getIsImage() {
        return isImage;
    }

    public void setIsImage(boolean image) {
        isImage = image;
    }

    public boolean getSingleAnswer() {
        return singleAnswer;
    }

    public void setSingleAnswer(boolean singleAnswer) {
        this.singleAnswer = singleAnswer;
    }

    public String getSelectedAnswer() {
        return selectedAnswer;
    }

    public void setSelectedAnswer(String selectedAnswer) {
        this.selectedAnswer = selectedAnswer;
    }

    public boolean getAnswered() {
        return answered;
    }

    public void setAnswered(boolean answered) {
        this.answered = answered;
    }
}
