package com.brijeshsonawane234.post.Gamification.Model;

import android.widget.Button;

public class ClickId
{
    int buttonId;
    int textId;

    public ClickId()
    {

    }

    public ClickId(int buttonId,int textId) {
        this.buttonId = buttonId;
        this.textId= textId;
    }

    public int getTextId() {
        return textId;
    }

    public void setTextId(int textId) {
        this.textId = textId;
    }

    public int getButtonId() {
        return buttonId;
    }

    public void setButtonId(int buttonId) {
        this.buttonId = buttonId;
    }
}
