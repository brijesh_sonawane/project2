package com.brijeshsonawane234.post.Gamification.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brijeshsonawane234.post.Gamification.Model.JSON;
import com.brijeshsonawane234.post.Gamification.Model.ObjectData;
import com.brijeshsonawane234.post.Gamification.Model.Option;
import com.brijeshsonawane234.post.Gamification.Model.OptionData;
import com.brijeshsonawane234.post.Gamification.Model.QuestionData;
import com.brijeshsonawane234.post.Gamification.Model.ScrabbleCharacters;
import com.brijeshsonawane234.post.Gamification.Model.ScrabbleQuestion;
import com.brijeshsonawane234.post.R;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ActivityQuiz extends AppCompatActivity
{
    TextView tvQuestion,tvOption1,tvOption1Text,tvOption2,tvOption2Text,tvOption3,tvOption3Text,tvOption4,tvOption4Text,tv_CurrntQuestion,tv_TotalQuestion;
    JSON json;
    int currentQuestion = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        tvQuestion=(TextView) findViewById(R.id.tvQuestion);
        tvOption1=(TextView) findViewById(R.id.tvOption1);
        tvOption1Text=(TextView) findViewById(R.id.tvOption1Text);
        tvOption2=(TextView) findViewById(R.id.tvOption2);
        tvOption2Text=(TextView) findViewById(R.id.tvOption2Text);
        tvOption3=(TextView) findViewById(R.id.tvOption3);
        tvOption3Text=(TextView) findViewById(R.id.tvOption3Text);
        tvOption4=(TextView) findViewById(R.id.tvOption4);
        tvOption4Text=(TextView) findViewById(R.id.tvOption4Text);
        tv_CurrntQuestion=(TextView) findViewById(R.id.currentQuestionId);
        tv_TotalQuestion=(TextView) findViewById(R.id.totalQuestionId);
        loadAllQuestions();
        setQuestionScreen(currentQuestion);

        tvOption1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                check(tvOption1);
                currentQuestion++;
                setQuestionScreen(currentQuestion);

            }
        });
        tvOption1Text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                check(tvOption1Text);
                currentQuestion++;
                setQuestionScreen(currentQuestion);
            }
        });

        tvOption2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                check(tvOption2);
                currentQuestion++;
                setQuestionScreen(currentQuestion);
            }
        });
        tvOption2Text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                check(tvOption2Text);
                currentQuestion++;
                setQuestionScreen(currentQuestion);
            }
        });

        tvOption3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                check(tvOption3);
                currentQuestion++;
                setQuestionScreen(currentQuestion);
            }
        });
        tvOption3Text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                check(tvOption3Text);
                currentQuestion++;
                setQuestionScreen(currentQuestion);
            }
        });

        tvOption4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                check(tvOption4);
                currentQuestion++;
                setQuestionScreen(currentQuestion);
            }
        });
        tvOption4Text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                check(tvOption4Text);
                currentQuestion++;
                setQuestionScreen(currentQuestion);
            }
        });
    }
    public void check(TextView tv)
    {
        String ansId=json.getObject().getQuestions().get(currentQuestion).getSelectedAnswer();
        String ans = null;
        for (OptionData op: json.getObject().getQuestions().get(currentQuestion).getOptions())
        {
            if (ansId.equals(op.getAnswerId()))
            {
                ans= String.valueOf(op.getAnswer());
                Log.d("op.getAns","-----"+op.getAnswer());
            }
        }

        if (tv.getText().equals(ans) || tv.getText().equals(ansId))
        {
            Log.w("true","----------------------------true"+ans+",,,,"+ansId);
        }
        else
        {
            Log.w("false","----------------------------false"+ans+",,,,"+ansId);
        }
    }

    //set question to screen
    public void setQuestionScreen(int count)
    {
        if (currentQuestion < 3)
        {
            tv_CurrntQuestion.setText(""+(currentQuestion+1));
            tvQuestion.setText(json.getObject().getQuestions().get(count).getQuestion());
            tvOption1.setText(json.getObject().getQuestions().get(count).getOptions().get(0).getAnswerId());
            tvOption1Text.setText("" + json.getObject().getQuestions().get(count).getOptions().get(0).getAnswer());
            tvOption2.setText(json.getObject().getQuestions().get(count).getOptions().get(1).getAnswerId());
            tvOption2Text.setText("" + json.getObject().getQuestions().get(count).getOptions().get(1).getAnswer());
            tvOption3.setText(json.getObject().getQuestions().get(count).getOptions().get(2).getAnswerId());
            tvOption3Text.setText("" + json.getObject().getQuestions().get(count).getOptions().get(2).getAnswer());
            tvOption4.setText(json.getObject().getQuestions().get(count).getOptions().get(3).getAnswerId());
            tvOption4Text.setText("" + json.getObject().getQuestions().get(count).getOptions().get(3).getAnswer());

        }
        else
        {
            startActivity(new Intent(ActivityQuiz.this, ActivityImageQuiz.class));
        }
    }

    //make the list with all question
    private void loadAllQuestions()
    {
        String jsonStr= loadJSONFromAsset();
        try
        {
            JSONObject jsonFile = new JSONObject(jsonStr);
            json=new JSON();
            json.setCode(jsonFile.getInt("code"));
            json.setStatus(jsonFile.getString("status"));

                JSONObject jsonFileObject = jsonFile.getJSONObject("object");
                ObjectData objectData=new ObjectData();
                objectData.setContestId(jsonFileObject.getInt("contestId"));

                    JSONArray jsonArrayQuestions = jsonFileObject.getJSONArray("questions");
                    List<QuestionData> questionDataList = new ArrayList<>();
                    for (int i=0;i<jsonArrayQuestions.length();i++)
                    {
                        JSONObject jsonQuestion = jsonArrayQuestions.getJSONObject(i);
                        QuestionData questionData=new QuestionData();
                        questionData.setQuestion(jsonQuestion.getString("question"));
                        questionData.setQuestionId(jsonQuestion.getString("questionId"));
                        questionData.setIsImage(jsonQuestion.getBoolean("isImage"));
                        questionData.setSingleAnswer(jsonQuestion.getBoolean("singleAnswer"));
                        questionData.setSelectedAnswer(jsonQuestion.getString("selectedAnswer"));
                        questionData.setAnswered(jsonQuestion.getBoolean("answered"));

                            JSONArray jsonOptions = jsonQuestion.getJSONArray("options");
                            List<OptionData> optionDataList = new ArrayList<>();
                            for (int j=0;j<jsonOptions.length();j++)
                            {
                                JSONObject jsonOption = jsonOptions.getJSONObject(j);
                                OptionData optionData = new OptionData();
                                optionData.setAnswerId(jsonOption.getString("answerId"));
                                optionData.setAnswer(jsonOption.getInt("answer"));
                                optionDataList.add(optionData);
                            }
                            questionData.setOptions(optionDataList);
                        questionDataList.add(questionData);
                    }
                objectData.setQuestions(questionDataList);
            json.setObject(objectData);

            Log.d("code","------------------"+json.getCode());
            Log.d("Status","------------------"+json.getStatus());
            Log.d("object","------------------"+json.getObject());
            Log.d("ContestId","------------------"+json.getObject().getContestId());
            Log.d("totalQuestion","*********"+json.getObject().getQuestions().size());
            tv_TotalQuestion.setText(""+json.getObject().getQuestions().size());
            for (QuestionData questionData: json.getObject().getQuestions())
            {
                Log.d("getQuestion","------------------"+questionData.getQuestion());
                Log.d("questionId","------------------"+questionData.getQuestionId());
                Log.d("isImage","------------------"+questionData.getIsImage());
                Log.d("singleAnswer","------------------"+questionData.getSingleAnswer());
                Log.d("selectedAnswer","------------------"+questionData.getSelectedAnswer());
                Log.d("answered","------------------"+questionData.getAnswered());
                for (OptionData optionData:questionData.getOptions())
                {
                    Log.d("answerId","------------------"+optionData.getAnswerId());
                    Log.d("answer","------------------"+optionData.getAnswer());
                }
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }


    private String loadJSONFromAsset()
    {
        //Log.d("infun","==    private String loadJSONFromAsset(String file)");
        String json="";
        try {
            InputStream is=getAssets().open("question_option.json");
            int size= is.available();
            Log.d("size","size === "+size);
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json=new String(buffer,"UTF-8");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        Log.d("json1","in fun"+json.length());
        return json;
    }

}
