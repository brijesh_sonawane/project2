package com.brijeshsonawane234.post.Gamification.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.brijeshsonawane234.post.R;

public class Main4Activity extends AppCompatActivity {

    private Toolbar toolbar;
    TextView tv_data,tv_result,tv_brandName;
    Spinner spin;
    String[] terms = { "Terms and Conditions" };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        toolbar= (Toolbar)findViewById(R.id.mytoolbar);
        tv_data=(TextView) findViewById(R.id.tv3);
        tv_result=(TextView) findViewById(R.id.tv4);
        tv_brandName=(TextView) findViewById(R.id.tv5);


        tv_data.setText("for your participation");
        tv_result.setText("Result will be sent you by SMS/email by <X days> - Lorem Ipsum is simply dummy text");
        tv_brandName.setText("Meanwhile,browse <Brand Name> products - Lorem Ipsum is simply dummy text");
        spin = (Spinner) findViewById(R.id.sp1);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.spinner_item_1, terms);
        adapter.setDropDownViewResource(R.layout.spinner_item_1);
        spin.setAdapter(adapter);
        setSupportActionBar(toolbar);
    }
    public void fun(View view)
    {

    }
}
