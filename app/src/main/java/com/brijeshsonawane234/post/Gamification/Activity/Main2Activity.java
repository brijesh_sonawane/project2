package com.brijeshsonawane234.post.Gamification.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brijeshsonawane234.post.Gamification.Model.Option;
import com.brijeshsonawane234.post.Gamification.Model.Question;
import com.brijeshsonawane234.post.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    TextView tv_question;
    ImageView imageViewHead;
    LinearLayout layout;
    List<Button> buttons = new ArrayList<>();
    int option=-1,flag = 0;
    List<Question> questions;
    String correctAns;
    int currentQuestion=0;
    int score=0;
    Timer timer=new Timer();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        layout = (LinearLayout) findViewById(R.id.rootLayout);
        toolbar= (Toolbar)findViewById(R.id.mytoolbar);
        tv_question = (TextView) findViewById(R.id.tv3);
        imageViewHead = (ImageView) findViewById(R.id.imageView1);
        setSupportActionBar(toolbar);

        //get all questions
        loadAllQuestions();
        //Collections.shuffle(questions);
        //load first question
        setQuestionScreen(currentQuestion);

        Log.d("Buttons",""+buttons.size());

    }

    private void setQuestionScreen(int number)
    {
        Log.d("setQue","setQue--"+currentQuestion);

        tv_question.setText(questions.get(number).getQuestion());
        correctAns=questions.get(number).getCorrect();
        for(int i=0;i<questions.get(number).getOption().size();i++)
        {
            addButton(questions.get(number).getOption().get(i).getAns());
        }

        timer.schedule(new TimerTask(){
            public void run() {
                Main2Activity.this.runOnUiThread(new Runnable() {
                    public void run() {

                        if (currentQuestion<questions.size()-1)
                        {

                            flag=1;
                            layout.removeAllViews();
                            currentQuestion++;
                            setQuestionScreen(currentQuestion);

                        }
                        else
                        {
                            Log.d("score","--"+score);
                            startActivity(new Intent(Main2Activity.this, Main4Activity.class));
                            Main2Activity.this.finish();
                        }

                    }
                });
            }
        }, 10000);

    }


    public void addButton(String op)
    {
        option++;
        Log.d("fun","fun");
        LinearLayout.LayoutParams layoutParams= new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(10,50,10,0);
        Button b= new Button(this);
        b.setText(op);
        b.setTextSize(15);
        b.setTypeface(Typeface.DEFAULT);
        b.setTextColor(Color.BLACK);
        b.setTransformationMethod(null);
        b.setLayoutParams(layoutParams);
        b.setBackgroundResource(R.drawable.button_border);
        b.setId(option);
        b.setOnClickListener((View.OnClickListener) this);
        Log.d("fun","Id = "+b.getId());
        layout.addView(b);
        buttons.add(b);
    }

    @Override
    public void onClick(View view)
    {
        Log.d("id","iD ==== "+view.getId());
        if (buttons.get(view.getId()).getText().equals(correctAns))
        {
            //Ans Right
            Log.d("onClick","Right");
            score++;
            //buttons.get(view.getId()).setBackgroundColor(Color.GREEN);
        }
        else
        {
            //Ans Wrong
            Log.d("onClick","Wrong");
            //buttons.get(view.getId()).setBackgroundColor(Color.RED);
        }
        for (Button b:buttons)
        {
            b.setEnabled(false);
        }
        timer.cancel();
        timer=new Timer();

        new Timer().schedule(new TimerTask(){
            public void run() {
                Main2Activity.this.runOnUiThread(new Runnable() {
                    public void run()
                    {
                        Log.d("setQuein onClick","onClickset--"+currentQuestion);
                        if (currentQuestion<questions.size()-1)
                        {
                            layout.removeAllViews();
                            currentQuestion++;
                            setQuestionScreen(currentQuestion);

                        }
                        else
                        {
                            Log.d("score","--00000"+score);
                            startActivity(new Intent(Main2Activity.this, Main4Activity.class));
                            Main2Activity.this.finish();
                        }
                    }
                });
            }
        }, 1000);




    }

    private void loadAllQuestions()
    {
        questions = new ArrayList<>();
        String jsonStr = loadJSONFromAsset("data.json");
        Log.d("json1","in fun1");
        try
        {
            JSONObject jsonObject =  new JSONObject(jsonStr);
            Log.d("SizeOfJsonObject","====="+jsonObject);
            JSONArray questionss = jsonObject.getJSONArray("questions");

            for (int i=0;i< questionss.length();i++)
            {
                Question questionObject=new Question();
                JSONObject question = questionss.getJSONObject(i);
                String que = question.getString("question");
                String correct = question.getString("correct");;
                Log.d("que","====="+que);
                Log.d("correct","====="+correct);

                List<Option> optionList= new ArrayList<>();
                JSONArray options = question.getJSONArray("option");
                Log.d("options","====="+options);
                for (int j=0;j<options.length();j++)
                {
                    Option optionObject=new Option();
                    JSONObject option = options.getJSONObject(j);
                    String ans=option.getString("ans");
                    optionObject.setAns(ans);
                    optionList.add(optionObject);
                }

                questionObject.setQuestion(que);
                questionObject.setCorrect(correct);
                questionObject.setOption(optionList);

                questions.add(questionObject);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String loadJSONFromAsset(String file)
    {
        Log.d("infun","==    private String loadJSONFromAsset(String file)");
        String json="";
        try {
            InputStream is=getAssets().open("data.json");
            int size= is.available();
            Log.d("size","size === "+size);
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json=new String(buffer,"UTF-8");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        Log.d("json1","in fun"+json.length());
        return json;
    }
}
