package com.brijeshsonawane234.post.Activity;


import android.os.Bundle;

import com.brijeshsonawane234.post.Adapter.ObjectDataAdapter;
import com.brijeshsonawane234.post.Model.ObjectData;
import com.brijeshsonawane234.post.R;
import com.brijeshsonawane234.post.viewmodel.ObjectDataViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity1 extends AppCompatActivity
{
    private RecyclerView recyclerView;
    private ObjectDataAdapter objectAdapter;
    private List<ObjectData> objectData=new ArrayList<>();
    List<ObjectData> data =null;
    ObjectDataViewModel objectDataViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        objectAdapter = new ObjectDataAdapter(objectData,this);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(objectAdapter);

        objectDataViewModel = ViewModelProviders.of(this).get(ObjectDataViewModel.class);
        objectData = objectDataViewModel.getObjectData(objectAdapter);
        objectAdapter.setObjectList(objectData);
        objectAdapter.notifyDataSetChanged();

        //prepareData();

    }
//    private void prepareData()
//    {
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("https://prod-b2c-api.taglr.com:8443/web-b2c/open/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        PostInfo postInfo = new PostInfo(false, "dc63df2df84f4264");
//        ApiRetrofit apiRetrofit = retrofit.create(ApiRetrofit.class);
//        Call<JSON> call = apiRetrofit.postData(postInfo);
//
//        call.enqueue(new Callback<JSON>() {
//            @Override
//            public void onResponse(Call<JSON> call, Response<JSON> response)
//            {
//                Log.d("response","success");
//                //Log.d("respons",response.body().toString());
//                JSON json=response.body();
//                Log.d("Json Status",json.getStatus());
//                Log.d("Json code",""+json.getCode());
//
//                data= json.getObjectData();
//
//                if(data != null)
//                {
//                    List<ObjectData> objectData = new ArrayList<>();
//                    Log.d("not null","not null");
//                    for (ObjectData o:data)
//                    {
//                        //Log.d("section header",o.getSection_header());
//                        if (o.getSection_header()!= null)
//                        {
//
//                            if (o.getTemplate_id() == 43 || o.getTemplate_id() == 41 && o.getSection_sequence() == 3)
//                            {
//                                Log.d("head", o.getSection_header());
//                                objectData.add(o);
//                            }
//                        }
//                    }
//                    objectAdapter.setObjectList(objectData);
//                }
//                else
//                {
//                    Log.d("null","null");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JSON> call, Throwable t)
//            {
//                Log.d("response","fail");
//
//            }
//        });
//
//        objectAdapter.notifyDataSetChanged();
//    }

}
