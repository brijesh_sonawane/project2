package com.brijeshsonawane234.post.Remot;

import com.brijeshsonawane234.post.Model.JSON;
import com.brijeshsonawane234.post.Model.PostInfo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiRetrofit
{
    @POST("homepage/homebanner")
    Call<JSON> postData(@Body PostInfo postInfo);
}
