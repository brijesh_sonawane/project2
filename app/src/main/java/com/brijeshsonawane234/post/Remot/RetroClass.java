package com.brijeshsonawane234.post.Remot;

import android.util.Log;

import com.brijeshsonawane234.post.Adapter.ObjectDataAdapter;
import com.brijeshsonawane234.post.Model.JSON;
import com.brijeshsonawane234.post.Model.ObjectData;
import com.brijeshsonawane234.post.Model.PostInfo;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClass
{
    private static final String rootUrl="https://prod-b2c-api.taglr.com:8443/web-b2c/open/";
    final public List<ObjectData> objectData = new ArrayList<>();

    private static Retrofit getRetrofitInstance()
    {

        return new Retrofit.Builder().baseUrl(rootUrl).addConverterFactory(GsonConverterFactory.create()).build();
    }

    public static ApiRetrofit getApiRetrofit()
    {

        return getRetrofitInstance().create(ApiRetrofit.class);
    }

    public List<ObjectData>  prepareData(final ObjectDataAdapter objectDataAdapter)
    {
        Log.d("prepare","prepareData");
        ApiRetrofit apiRetrofit = RetroClass.getApiRetrofit();

        PostInfo postInfo = new PostInfo(false, "dc63df2df84f4264");

        Log.d("prepare1","prepareData1");
        apiRetrofit.postData(postInfo).enqueue(new Callback<JSON>() {
            @Override
            public void onResponse(Call<JSON> call, Response<JSON> response)
            {

                Log.d("response","success");

                JSON json=response.body();

                final List<ObjectData>  data= json.getObjectData();

                if (data != null)
                {

                    for (ObjectData o:data)
                    {
                        if (o.getSection_header()!= null)
                        {

                            if (o.getTemplate_id() == 43 || o.getTemplate_id() == 41 && o.getSection_sequence() == 3)
                            {
                                Log.d("head", o.getSection_header());
                                objectData.add(o);
                            }
                        }
//                        Log.d("size in for",""+objectData.size());
                    }
//                  Log.d("size in if",""+objectData.size());
                    objectDataAdapter.setObjectList(objectData);

                }
                else
                {
                    Log.d("null","null");
                }
            }

            @Override
            public void onFailure(Call<JSON> call, Throwable t)
            {
                Log.d("fail","fail");
            }
        });

        Log.d("sizeGetObject",""+objectData.size());
        return objectData;
    }

}
