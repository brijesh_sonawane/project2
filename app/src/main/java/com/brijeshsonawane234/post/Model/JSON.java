package com.brijeshsonawane234.post.Model;

import java.io.Serializable;
import java.util.List;

public class JSON  implements Serializable {
    int code;
    String status;
    List<ObjectData> object;

    public JSON(int code, String status, List<ObjectData> objectData) {
        this.code = code;
        this.status = status;
        this.object = objectData;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ObjectData> getObjectData() {
        return object;
    }

    public void setObjectData(List<ObjectData> objectData) {
        this.object = objectData;
    }
}
