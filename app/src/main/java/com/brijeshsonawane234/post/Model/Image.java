package com.brijeshsonawane234.post.Model;

import java.io.Serializable;

public class Image implements Serializable
{
    String image_url;
    String retailer_id;
    String image_id;
    String redirect_url;

    public Image(String image_url, String retailer_id, String image_id, String redirect_url) {
        this.image_url = image_url;
        this.retailer_id = retailer_id;
        this.image_id = image_id;
        this.redirect_url = redirect_url;
    }

    public Image(String image_url, String retailer_id, String image_id) {
        this.image_url = image_url;
        this.retailer_id = retailer_id;
        this.image_id = image_id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getRetailer_id() {
        return retailer_id;
    }

    public void setRetailer_id(String retailer_id) {
        this.retailer_id = retailer_id;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getRedirect_url() {
        return redirect_url;
    }

    public void setRedirect_url(String redirect_url) {
        this.redirect_url = redirect_url;
    }
}
