package com.brijeshsonawane234.post.Model;

import java.io.Serializable;

public class PostInfo implements Serializable
{
    Boolean offline_banners;
    String visitorId;

    JSON json;
    public PostInfo(Boolean offline_banners, String visitorId) {
        this.offline_banners = offline_banners;
        this.visitorId = visitorId;
    }

    public Boolean getOffline_banners() {
        return offline_banners;
    }

    public void setOffline_banners(Boolean offline_banners) {
        this.offline_banners = offline_banners;
    }

    public String getVisitorId() {
        return visitorId;
    }

    public void setVisitorId(String visitorId) {
        this.visitorId = visitorId;
    }

    public JSON getJson() {
        return json;
    }

    public void setJson(JSON json) {
        this.json = json;
    }

}
