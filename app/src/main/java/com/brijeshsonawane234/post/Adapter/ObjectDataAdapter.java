package com.brijeshsonawane234.post.Adapter;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brijeshsonawane234.post.Activity.MainActivity1;
import com.brijeshsonawane234.post.Model.Data;
import com.brijeshsonawane234.post.Model.Image;
import com.brijeshsonawane234.post.Model.ObjectData;
import com.brijeshsonawane234.post.R;
import com.bumptech.glide.Glide;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ObjectDataAdapter extends RecyclerView.Adapter
{
    List<ObjectData> objectDataList;
    MainActivity1 mainActivity;

    public ObjectDataAdapter(List<ObjectData> objectDataList, MainActivity1 mainActivity)
    {

        this.objectDataList = objectDataList;
        this.mainActivity=mainActivity;
    }

    @Override
    public int getItemViewType(int position)
    {
        ObjectData objectData= objectDataList.get(position);
        if (objectData.getSection_header().equals("Trending Categories"))
        {
            Log.d("Trending Categories","Trending Categories");
            return 0;
        }
        Log.d("Music on My Mind! ","Music on My Mind! ");
        return 1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.row,parent,false);
//        return new MyViewHolder(itemView);
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view;
        if (viewType == 1)
        {
            view = layoutInflater.inflate(R.layout.row,parent,false);
            return new MyViewHolder(view);
        }
        view = layoutInflater.inflate(R.layout.row_trending_categories,parent,false);
        return new ViewHolderTrendingCategories(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        ObjectData objectData= objectDataList.get(position);
        if (objectData.getSection_header().equals("Music on My Mind! "))
        {
            if (holder instanceof MyViewHolder)
            {
                Log.d("instanceOd","MyViewHilder");
            }
            else
            {
                Log.d("instanceOd","ViewHolderTrendingCategories");
            }
            MyViewHolder viewHolder = (MyViewHolder) holder;
            List<Data> data= objectData.getData();
            List<Image> images=null;
            int count=0;
            for (Data d: data)
            {
                if (d.getImages() != null)
                {
                    Log.d("data1",""+d.getImages().length);
                    viewHolder.textView.setText(objectData.getSection_header());
                    images= Arrays.asList(d.getImages());
                    if (count == 0)
                    {
                        Glide.with(mainActivity).load(images.get(0).getImage_url()).into(viewHolder.imageView1);
                        viewHolder.textView1.setText(d.getLabel().toUpperCase());
                    }
                    else
                    {
                        if (count == 1)
                        {
                            Glide.with(mainActivity).load(images.get(0).getImage_url()).into(viewHolder.imageView2);
                            viewHolder.textView2.setText(d.getLabel().toUpperCase());
                        }
                        else
                        {
                            Glide.with(mainActivity).load(images.get(0).getImage_url()).into(viewHolder.imageView3);
                        }
                    }
                }
                count++;
            }
        }
        else
        {
            if (holder instanceof MyViewHolder)
            {
                Log.d("instanceOd","else MyViewHilder");
            }
            else
            {
                Log.d("instanceOd","else ViewHolderTrendingCategories");
            }
            ViewHolderTrendingCategories viewHolder = (ViewHolderTrendingCategories) holder;
            List<Data> data= objectData.getData();
            List<Image> images=null;
            int count=0;
            for (Data d: data)
            {

                if (d.getImages() != null)
                {
                    Log.d("data1",""+d.getImages().length);
                    viewHolder.textView.setText(objectData.getSection_header());
                    images= Arrays.asList(d.getImages());
                    if (count == 0)
                    {
                        Glide.with(mainActivity).load(images.get(0).getImage_url()).into(viewHolder.imageView1);
                    }
                    else
                    {

                        if (count == 1)
                        {
                            Glide.with(mainActivity).load(images.get(0).getImage_url()).into(viewHolder.imageView2);
                        }
                        else
                        {
                            if(count == 2)
                            {
                                Glide.with(mainActivity).load(images.get(0).getImage_url()).into(viewHolder.imageView3);
                            }
                            else
                            {
                                Glide.with(mainActivity).load(images.get(0).getImage_url()).into(viewHolder.imageView4);
                            }

                        }
                    }

                }
                count++;
            }

        }
    }

//    @Override
//    public void onBindViewHolder(@NonNull MyViewHolder holder, int position)
//    {
//        ObjectData objectData= objectDataList.get(position);
//        List<Data> data= objectData.getData();
//        List<Image> images=null;
//        int count=0;
//        for (Data d: data)
//        {
//
//            if (d.getImages() != null)
//            {
//                Log.d("data1",""+d.getImages().length);
//                holder.textView.setText(objectData.getSection_header());
//                images= Arrays.asList(d.getImages());
//                if (count == 0)
//                {
//                    Glide.with(mainActivity).load(images.get(0).getImage_url()).into(holder.imageView1);
//                    holder.textView1.setText(d.getLabel().toUpperCase());
//                }
//                else
//                {
//
//                    if (count == 1)
//                    {
//                        Glide.with(mainActivity).load(images.get(0).getImage_url()).into(holder.imageView2);
//                        holder.textView2.setText(d.getLabel().toUpperCase());
//                    }
//                    else
//                    {
//                        Glide.with(mainActivity).load(images.get(0).getImage_url()).into(holder.imageView3);
//                    }
//                }
//
//            }
//            count++;
//        }
//    }

    @Override
    public int getItemCount()
    {
        Log.d("size",""+objectDataList.size());
        return objectDataList.size();

    }


    public void  setObjectList(List<ObjectData>objectList )
    {
        this.objectDataList = objectList;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView sectionHeader;
        ImageView imageView1,imageView2,imageView3;
        TextView textView,textView1,textView2;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView1=(ImageView) itemView.findViewById(R.id.imageView1);
            imageView2=(ImageView) itemView.findViewById(R.id.imageView2);
            imageView3=(ImageView) itemView.findViewById(R.id.imageView3);
            textView1=(TextView) itemView.findViewById(R.id.tv1);
            textView2=(TextView) itemView.findViewById(R.id.tv2);
            textView=(TextView) itemView.findViewById(R.id.tv3);
        }
    }
    public class ViewHolderTrendingCategories extends RecyclerView.ViewHolder
    {
        ImageView imageView1,imageView2,imageView3,imageView4;
        TextView textView;
        public ViewHolderTrendingCategories(@NonNull View itemView)
        {
            super(itemView);
            imageView1=(ImageView) itemView.findViewById(R.id.imageView1);
            imageView2=(ImageView) itemView.findViewById(R.id.imageView2);
            imageView3=(ImageView) itemView.findViewById(R.id.imageView3);
            imageView4=(ImageView) itemView.findViewById(R.id.imageView4);
            textView=(TextView) itemView.findViewById(R.id.tv1);
        }
    }

}
